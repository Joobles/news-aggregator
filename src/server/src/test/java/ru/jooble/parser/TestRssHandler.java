package ru.jooble.parser;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;
import ru.jooble.model.News;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class TestRssHandler {
    private SAXParserFactory factory = SAXParserFactory.newInstance();

    @Test
    public void testParser() throws ParserConfigurationException, SAXException, IOException {
        SAXParser saxParser = this.factory.newSAXParser();
        RssHandler rssHandler = new RssHandler();
        File rss = new File("src/test/resources/vesti.xml");

        saxParser.parse(rss, rssHandler);
        List<News> result = rssHandler.getResults();
        Assert.assertTrue(result.isEmpty());
    }

}
