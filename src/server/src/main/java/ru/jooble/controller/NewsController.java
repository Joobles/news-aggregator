package ru.jooble.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.jooble.model.News;
import ru.jooble.service.NewsService;

import java.util.List;

@Controller
@RestController
public class NewsController {

    @Autowired
    NewsService newsService;

    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public List<News> getNews() {
        return newsService.getLimitNews(20);
    }

    @RequestMapping(value = "/news/{from}", method = RequestMethod.GET)
    public List<News> getNews(@PathVariable("from") int from) {
        return newsService.getLimitNews(from, 20);
    }

    @RequestMapping(value = "/new/{id}", method = RequestMethod.GET)
    public News getNew(@PathVariable("id") Long id) {
        return newsService.findOne(id);
    }
}