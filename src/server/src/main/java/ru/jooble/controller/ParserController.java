package ru.jooble.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.jooble.model.RssNews;
import ru.jooble.parser.RssParser;
import ru.jooble.service.RssNewsService;

import java.util.List;

@Controller
@RestController
public class ParserController {

    @Autowired
    private RssParser rssParser;
    @Autowired
    private RssNewsService rssNewsService;

    @RequestMapping(value = "/parsing", method = RequestMethod.GET)
    public List<RssNews> parse() {
        List<RssNews> rssNewsList = rssNewsService.findAll();

        rssNewsList.forEach(rssParser::rssProcessing);

        return rssNewsList;
    }
}
