package ru.jooble.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
@PropertySource("classpath:config.properties")
public class ThreadPoolConfig {

    @Autowired
    private Environment environment;

    @Bean
    public ExecutorService executorService() {
        int threadPoolSize = Integer.parseInt(environment.getProperty("thread.pool.size"));
        return Executors.newFixedThreadPool(threadPoolSize);
    }
}
