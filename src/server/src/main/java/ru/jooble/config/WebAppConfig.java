package ru.jooble.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackageClasses = {ru.jooble.controller.RepositoryPackageMarker.class,
        ru.jooble.service.RepositoryPackageMarker.class, ru.jooble.parser.RepositoryPackageMarker.class})
public class WebAppConfig extends WebMvcConfigurerAdapter {
}
