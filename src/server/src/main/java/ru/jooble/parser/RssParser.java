package ru.jooble.parser;

import ru.jooble.model.RssNews;

public interface RssParser {

    void rssProcessing(RssNews rssNews);
}
