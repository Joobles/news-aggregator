package ru.jooble.parser.net;

import java.io.IOException;
import java.io.InputStream;

public interface FileDownloader {

    InputStream download(String link) throws IOException;
}
