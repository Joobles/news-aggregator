package ru.jooble.parser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.jooble.model.News;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class RssHandler extends DefaultHandler {
    private static final String ITEM = "ITEM";
    private static final String TITLE = "TITLE";
    private static final String LINK = "LINK";
    private static final String DESCRIPTION = "DESCRIPTION";
    private static final String PUBDATE = "PUBDATE";
    private static final String CATEGORY = "CATEGORY";
    private boolean qTitle;
    private StringBuilder bTitle = new StringBuilder();
    private boolean qLink;
    private StringBuilder bLink = new StringBuilder();
    private boolean qDescription;
    private StringBuilder bDescription = new StringBuilder();
    private boolean qPubDate;
    private StringBuilder bPubDate = new StringBuilder();
    private boolean qCategory;
    private StringBuilder bCategory = new StringBuilder();

    private News news;
    private List<News> results = new LinkedList<>();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //TODO обработка ошибок
        switch (qName.toUpperCase()) {
            case ITEM:
                this.news = new News();
                break;
        }
        if (this.news != null) {
            switch (qName.toUpperCase()) {
                case TITLE:
                    this.qTitle = true;
                    break;
                case LINK:
                    this.qLink = true;
                    break;
                case DESCRIPTION:
                    this.qDescription = true;
                    break;
                case PUBDATE:
                    this.qPubDate = true;
                    break;
                case CATEGORY:
                    this.qCategory = true;
                    break;
            }
        }
    }


    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (news != null) {
            switch (qName.toUpperCase()) {
                case TITLE:
                    this.news.setTitle(this.bTitle.toString());
                    this.bTitle.setLength(0);
                    this.qTitle = false;
                    break;
                case LINK:
                    this.news.setLink(this.bLink.toString());
                    this.bLink.setLength(0);
                    this.qLink = false;
                    break;
                case DESCRIPTION:
                    this.news.setDescription(this.bDescription.toString());
                    this.bDescription.setLength(0);
                    this.qDescription = false;
                    break;
                case PUBDATE:
                    this.news.setPubDate(stringToLocalDateTime(this.bPubDate.toString()));
                    this.bPubDate.setLength(0);
                    this.qPubDate = false;
                    break;
                case CATEGORY:
                    this.news.setCategory(this.bCategory.toString());
                    this.bCategory.setLength(0);
                    this.qCategory = false;
                    break;
                case ITEM:
                    this.results.add(news);
                    break;
            }
        }
    }


    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        if (qTitle) {
            this.bTitle.append(ch, start, length);
        } else if (qLink) {
            this.bLink.append(ch, start, length);
        } else if (qDescription) {
            this.bDescription.append(ch, start, length);
        } else if (qPubDate) {
            this.bPubDate.append(ch, start, length);
        } else if (qCategory) {
            this.bCategory.append(ch, start, length);
        }
    }

    private LocalDateTime stringToLocalDateTime(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
        return LocalDateTime.parse(date, formatter);
    }

    public List<News> getResults() {
        return results;
    }

    public void setResults(List<News> results) {
        this.results = results;
    }
}
