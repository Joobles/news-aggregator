package ru.jooble.parser.net;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class FileDownloaderImpl implements FileDownloader {
    @Override
    public InputStream download(String link) throws IOException {
        URL url = new URL(link);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        return conn.getInputStream();
    }
}
