package ru.jooble.parser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;
import ru.jooble.model.News;
import ru.jooble.model.RssNews;
import ru.jooble.parser.net.FileDownloader;
import ru.jooble.service.NewsService;
import ru.jooble.service.RssNewsService;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;

@Component
public class RssParserImpl implements RssParser {
    private SAXParserFactory factory = SAXParserFactory.newInstance();

    @Autowired
    private ExecutorService executorService;
    @Autowired
    private FileDownloader fileDownloader;
    @Autowired
    private NewsService newsService;
    @Autowired
    private RssNewsService rssNewsService;

    @Override
    public void rssProcessing(RssNews rssNews) {
        this.executorService.submit(() -> {
            try (InputStream inputStream = this.fileDownloader.download(rssNews.getLink())) {
                SAXParser saxParser = this.factory.newSAXParser();
                RssHandler rssHandler = new RssHandler();

                saxParser.parse(inputStream, rssHandler);
                List<News> result = rssHandler.getResults();

                result.stream().filter(news -> rssNews.getLastParsing().isBefore(news.getPubDate()))
                        .forEach(this.newsService::save);

                updateLastParsing(rssNews, result);
            } catch (ParserConfigurationException | SAXException | IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void updateLastParsing(RssNews rssNews, List<News> result) {
        if (result.isEmpty()) return;
        rssNews.setLastParsing(Collections.max(result,
                (a, b) -> a.getPubDate().compareTo(b.getPubDate())).getPubDate());
        this.rssNewsService.save(rssNews);
    }
}
