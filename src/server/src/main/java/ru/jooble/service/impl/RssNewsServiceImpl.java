package ru.jooble.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.jooble.dao.RssNewsDao;
import ru.jooble.model.RssNews;
import ru.jooble.service.RssNewsService;

import java.util.List;

@Component
public class RssNewsServiceImpl implements RssNewsService {
    @Autowired
    private RssNewsDao rssNewsDao;

    @Override
    public RssNews findOne(Long id) {
        return rssNewsDao.findOne(id);
    }

    @Override
    public void save(RssNews rssNews) {
        rssNewsDao.save(rssNews);
    }

    @Override
    public List<RssNews> findAll() {
        return rssNewsDao.findAll();
    }

    @Override
    public void delete(Long id) {
        rssNewsDao.delete(id);
    }
}
