package ru.jooble.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jooble.dao.NewsDao;
import ru.jooble.model.News;
import ru.jooble.service.NewsService;

import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {
    @Autowired
    private NewsDao newsDao;

    @Override
    @Transactional(readOnly = true)
    public News findOne(Long id) {
        return newsDao.findOne(id);
    }

    @Override
    @Transactional
    public void save(News news) {
        newsDao.save(news);
    }

    @Override
    @Transactional(readOnly = true)
    public List<News> findAll() {
        return newsDao.findAll();
    }

    @Override
    @Transactional
    public void delete(Long id) {
        newsDao.delete(id);
    }


    @Override
    public List<News> getLimitNews(int maxResult) {
        Pageable pageable = new PageRequest(0, maxResult);
        Page<News> page = newsDao.findAllByOrderByIdAsc(pageable);
        return page.getContent();
    }

    @Override
    public List<News> getLimitNews(int from, int maxResult) {
        Pageable pageable = new PageRequest(from, maxResult);
        Page<News> page = newsDao.findAllByOrderByIdAsc(pageable);
        return page.getContent();
    }
}
