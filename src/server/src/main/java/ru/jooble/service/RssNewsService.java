package ru.jooble.service;

import ru.jooble.model.RssNews;

import java.util.List;

public interface RssNewsService {
    RssNews findOne(Long id);

    void save(RssNews rssNews);

    List<RssNews> findAll();

    void delete(Long id);
}
