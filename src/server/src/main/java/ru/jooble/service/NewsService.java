package ru.jooble.service;

import ru.jooble.model.News;

import java.util.List;

public interface NewsService {

    News findOne(Long id);

    void save(News news);

    List<News> findAll();

    void delete(Long id);

    List<News> getLimitNews(int maxResult);

    List<News> getLimitNews(int from, int maxResult);
}
