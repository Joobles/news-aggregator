package ru.jooble.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "rss_news")
public class RssNews extends BaseEntity {
    private String link;

    @Column(name = "last_parsing")
    private LocalDateTime lastParsing;

    public String getLink() {
        return link;
    }

    public RssNews setLink(String link) {
        this.link = link;
        return null;
    }

    public LocalDateTime getLastParsing() {
        return lastParsing;
    }

    public void setLastParsing(LocalDateTime lastParsing) {
        this.lastParsing = lastParsing;
    }
}
