package ru.jooble.dao;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.jooble.model.News;

public interface NewsDao extends JpaRepository<News, Long> {

    Page<News> findAllByOrderByIdAsc(Pageable pageable);
}
