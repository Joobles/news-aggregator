package ru.jooble.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.jooble.model.RssNews;

public interface RssNewsDao extends JpaRepository<RssNews, Long> {
}
