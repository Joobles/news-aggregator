CREATE TABLE news (
  id          INTEGER      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title       VARCHAR(255) NOT NULL,
  link        VARCHAR(255) NOT NULL,
  description TEXT         NOT NULL,
  pub_date    DATETIME     NOT NULL,
  category    VARCHAR(255)
);

CREATE TABLE rss_news (
  id           INTEGER      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  link         VARCHAR(255) NOT NULL,
  last_parsing DATETIME     NOT NULL
);


INSERT INTO rss_news (link, last_parsing) VALUES ('https://life.ru/xml/feed.xml?hashtag=life78', '2016:11:03 16:18:00');

INSERT INTO rss_news (link, last_parsing) VALUES ('http://www.vesti.ru/vesti.rss', '2016:11:03 16:18:00');

/*INSERT INTO news (title, link, description, pub_date, category)
VALUES ('Полицейский обыскал задержанного вниз головой.', 'http://www.vesti.ru/doc.html?id=2817760', 'В Бразилии
полицейский применил необычный метод задержания.
 Он поднял арестованного молодого человека над головой и попытался вытряхнуть из него запрещенные вещества.
  Инцидент был запечатлен на видео, стража порядка подвергнут служебному расследованию.', '2016:11:03 16:18:00', 'В
  мире');

INSERT INTO news (title, link, description, pub_date, category)
VALUES ('Верховный суд предлагает запретить аресты предпринимателей', 'http://www.vesti.ru/doc.html?id=2817766',
        'Бизнесменов не должны заключать под стражу за преступления в сфере предпринимательской деятельности.
        Такое мнение высказали на пленуме в Верховном суде РФ.', '2016:11:03 16:59:00', 'Общество');

INSERT INTO news (title, link, description, pub_date, category)
VALUES ('Украина отказалась от сланцевого газа',
        'http://www.vesti.ru/doc.html?id=2817767',
        'Украина не намерена заниматься сланцевым газом,
         но планирует наращивать добычу голубого топлива, развивая традиционные способы. Об этом заявил председатель
          правления &quot;Нафтогаза&quot; Андрей Коболев.',
        '2016:11:03 16:33:00', 'В мире');*/